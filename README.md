# neural-graphs-mice-development

Comparing the structure of neural connections during the development of mice.

[![license][license-badge]][license]

## Project status

Ongoing

## Installation

Create a new conda environment for this project by running the following on your Terminal:

```sh
conda env create -f environment.yml
```

Activate the new environment with `conda activate ngmd` (or `source activate ngmd`, depending on your system) and install the necessary dependencies with

```sh
make install
```

Alternatively, you may explicitly run `pip3 install .`

### Further steps (optional)

Check the README files in `notebooks/` and `scripts/` for further instructions, if you want to be able to run the files therein.

## Usage

TBD.

## Contributing

See [CONTRIBUTING][contributing].

## Authors and acknowledgment

This project is a result of a collaboration between [CeDA][ceda] and [Flavio Donato's research group][flavio-donato] at the University of Basel.

## License

This project is released under the [BSD 3-Clause License][license].

[ceda]: https://ceda.unibas.ch/
[contributing]: CONTRIBUTING.md
[flavio-donato]: https://www.biozentrum.unibas.ch/facilities/services/services-a-z/overview/unit/research-group-flavio-donato
[license]: LICENSE
[license-badge]: https://img.shields.io/badge/license-BSD-green
