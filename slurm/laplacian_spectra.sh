#!/bin/bash

#Name of the job
#SBATCH --job-name=laplacian_spectra

#Number of CPU cores reserved
#SBATCH --cpus-per-task=4

#Memory reserved
#SBATCH --mem=1G

#Create array job to execute the same script with different parameters
#SBATCH --array=1-2

#Time during which the task will run
#SBATCH --time=00:30:00
#SBATCH --qos=30min

#Paths to STDOUT or STDERR files should be absolute or relative to the current
#working directory
#SBATCH --output=logs/laplacian_spectra.o
#SBATCH --error=logs/laplacian_spectra.e

#Notify via email when the task ends or fails
#SBATCH --mail-type=END,FAIL,TIME_LIMIT

#This job runs from the current working directory

#Load the required modules
##########################
eval "$(conda shell.bash hook)"
conda activate ngmd

#Export the required environment variables
##########################################
array_job_cmds=slurm/laplacian_spectra_cmds.sh

#Run the desired commands
#########################
# Compute Laplacian spectra of graphs at various thresholds. First, do that
# with sim_type == 'corrs', then with sim_type == 'zscores'.
eval $(sed -n ${SLURM_ARRAY_TASK_ID}p $array_job_cmds)
