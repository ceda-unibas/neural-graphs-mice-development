#!/bin/bash

#Name of the job
#SBATCH --job-name=structural_consistency

#Number of CPU cores reserved
#SBATCH --cpus-per-task=20

#Memory reserved
#SBATCH --mem=64G

#Create array job to execute the same script with different parameters
#SBATCH --array=1-5

#Time during which the task will run
#SBATCH --time=24:00:00
#SBATCH --qos=1day

#Paths to STDOUT or STDERR files should be absolute or relative to the current
#working directory
#SBATCH --output=logs/structural_consistency_%a.o
#SBATCH --error=logs/structural_consistency_%a.e

#Notify via email when the task ends or fails
#SBATCH --mail-type=END,FAIL,TIME_LIMIT

#This job runs from the current working directory

#Load the required modules
##########################
eval "$(conda shell.bash hook)"
conda activate ngmd

#Export the required environment variables
##########################################
array_job_cmds=slurm/structural_consistency_cmds.sh

#Run the desired commands
#########################
# Gather network summaries for graphs build at different thresholds, first with
eval $(sed -n ${SLURM_ARRAY_TASK_ID}p $array_job_cmds)
