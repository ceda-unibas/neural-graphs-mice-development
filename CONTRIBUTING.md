# Contributing

Contributions are welcome and appreciated. The development of this package takes place on [GitLab][neural-graphs-mice-development]. Issues, bugs, and feature requests should be reported there. Code and documentation can be improved by submitting a pull request. Please add documentation for any new code snippet. You can improve or add functionality in the repository by forking from main, developing your code, and then creating a pull request.

[neural-graphs-mice-development]: https://gitlab.com/ceda-unibas/neural-graphs-mice-development
