{
 "cells": [
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Loading data and creating graphs\n",
    "\n",
    "How to load the data made available by our collaborators, and how to create correlation-based, neural activity networks from it.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from datetime import datetime\n",
    "\n",
    "import matplotlib.pyplot as plt\n",
    "import nb_config\n",
    "import numpy as np\n",
    "\n",
    "from ngmd import dataset, graphs, plotting, sttc\n",
    "\n",
    "%reload_ext autoreload\n",
    "%autoreload 2\n",
    "\n",
    "# Dataset path as a global variable. Remember to set the correct path to the\n",
    "# dataset in `notebooks/nb_config.py`\n",
    "DATA_DIR = nb_config.DATA_DIR"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Creating an `AnimalDataset` object to interact with\n",
    "\n",
    "The dataset folder is organized around animal IDs. Under every animal ID folder, there are folders corresponding to different experiment sessions with the same animal. The sessions have a date associated to them. Rather than dealing with directory paths directly, we can instantiate an `AnimalDataset` object which makes it easier to interact with the dataset.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "animal_dataset = dataset.AnimalDataset(DATA_DIR)\n",
    "for animal in animal_dataset.animals:\n",
    "    print(animal.animal_id)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "number_of_sessions = 0\n",
    "for animal in animal_dataset.animals:\n",
    "    number_of_sessions += len(animal.sessions)\n",
    "print(\"Total number of sessions in the dataset: {}\".format(number_of_sessions))"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let us grab a sample animal and a corresponding sample session to examine. The `Session` object retains information about which animal it belongs to, as well as supplementary info, such as the date the experiment took place.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sample_animal = animal_dataset.get_animal_from_id(\"DON-006084\")\n",
    "sample_session = sample_animal.get_session_from_date(\"20210519\")\n",
    "print(\"Animal ID: {}\".format(sample_session.animal_id))\n",
    "print(\"Session date: {}\".format(datetime.strptime(sample_session.date, \"%Y%m%d\")))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Loading neural activity data\n",
    "\n",
    "Binary traces of neural activity were precomputed from raw calcium imaging data. Each trace is a sequence of rectangular, 1-valued pulsed representing a detected calcium pulse. Load the traces with `clean=True` to automatically remove traces the traces of \"bad\" ROIs.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "binarized_traces = sample_session.load_binarized_traces(clean=True)\n",
    "print(\"Binarized traces shape: {}\".format(binarized_traces.shape))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ax = plotting.plot_binary_traces(\n",
    "    binarized_traces, sample_rate=sample_session.sample_rate\n",
    ")\n",
    "ax.set_title(\n",
    "    \"Binary activity traces of {}/{}\".format(\n",
    "        sample_session.animal_id, sample_session.date\n",
    "    )\n",
    ")\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can plot an extract of a single trace to see that the pulses have a width to them, and that the trace is a sequence of 1s and 0s.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Plot a single trace\n",
    "trace = binarized_traces[0][2000:4000]\n",
    "t = np.arange(0, len(trace)) / sample_session.sample_rate\n",
    "fig, ax = plt.subplots(figsize=(20, 5))\n",
    "ax.plot(t, trace)\n",
    "ax.set_title(\"Single binary trace\")\n",
    "ax.set_xlabel(\"Time (s)\")\n",
    "plt.show()"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Creating a graph from a precomputed correlation matrix (or other neuronal similarity measurements)\n"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Session folders have precomputed correlation-based similarity matrices stored in them. Those precomputed matrices are the result of several pre-processing steps, including de-duplication of neuronal cells. It could be convenient then, if possible, to work with these pre-computed matrices directly.\n",
    "\n",
    "Let us first load a simple correlation matrix for the neurons of our sample session.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "corrs = sample_session.load_corrs()\n",
    "plt.imshow(corrs)\n",
    "plt.colorbar()\n",
    "plt.show()"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can supply this loaded matrix to the `NeuralActivityGraph`, setting a connectivity threshold of `0.3`. In doing so, we obtain the following graph. Note that this graph is different than the one we had displayed before, meaning that the correlation values that we loaded are different than the ones we computed directly from the filtered fluorescence signals. To see how the loaded correlation values were precomputed, read the dataset description.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Get a graph from the correlation matrix\n",
    "nag = graphs.NeuralActivityGraph(corr_mat=corrs, connect_thresh=0.2)\n",
    "\n",
    "# Choose which (sub)graph to plot\n",
    "# G = nag.G_nx  # The whole graph\n",
    "G = graphs.largest_cc(nag.G_nx)  # Largest connected component\n",
    "\n",
    "# Plot the graph\n",
    "plotting.plot_nx_graph(G)\n",
    "plt.show()"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `NeuralActivityGraph` inherits from the more general `graphs.SimilarityGraph` class. This means tht we can also be more general in creating graphs by instantiating a `SimilarityGraph` object and providing to it any similarity matrix relating the neurons that we are interested in.\n",
    "\n",
    "Towards this goal, let us load a pre-computed z-score matrix stored in the sample session's directory. These z-scores were pre-computed as follows. For each pair of neurons,one first computes the correlation coefficient between their calcium fluorescence time-series. Then, one produces several random circular shifts of the time-series of one of the neurons and computes the correlation coefficient between the unchanged time-series and the\n",
    "shifted ones. The z-score is then computed as the number of standard deviations away from the mean of the original correlation coefficient\n",
    "in the distribution of correlation coefficients obtained from the random circular shifts. One may interpret a high z-score as a sign of a functional similarity between the two corresponding neurons, as it means that these two neurons are more correlated than what would be expected by chance among\n",
    "all possible circular shifts of the neurons' calcium fluorescence time-series.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "z_scores = sample_session.load_zscores()\n",
    "plt.imshow(z_scores)\n",
    "plt.colorbar()\n",
    "plt.show()"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If we provide these z-scores as a similarity matrix to a `SimilarityGraph`, we get the following network. Note that a z-score network will most certainly have many more connections than a correlation network. Why? Because even if two neurons have very small correlation, they can still be connected, as long as that value is significantly larger than background noise correlations. In particular, we have to set a very high connectivity threshold to get numbers of isolated nodes close to the ones in the correlation-based network.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Get a similarity graph from the z-scores\n",
    "sg = graphs.SimilarityGraph(z_scores, connect_thresh=5)\n",
    "\n",
    "# Choose the (sub)graph to plot\n",
    "# G = sg.G_nx  # Whole graph\n",
    "G = graphs.largest_cc(sg.G_nx)  # Largest connected component\n",
    "\n",
    "# Plot the graph\n",
    "plotting.plot_nx_graph(G)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Creating graphs from Spike Time Tiling Coefficients (STTC)\n",
    "\n",
    "An STTC [Cutts & Eglen 2014](https://www.jneurosci.org/content/34/43/14288.short) is a pairwise measure of correlation between spike trains with the following properties:\n",
    "\n",
    "- it's not confounded by firing rate,\n",
    "- it appropriately distinguishes lack of correlation from anti-correlation,\n",
    "- periods of silence don't add to the correlation,\n",
    "- it's sensitive to firing patterns.\n",
    "\n",
    "The STTC computation in this repository follows a standalone fast vectorized implementation (see `ngmd.sttc`). You can feed a binary trace array (as loaded from `Session.load_binarized_traces()`), along with the session's sample rate, and a synchronicity window size parameter to `ngmd.timeseries.sttc_array()` and get in return a symmetric matrix of STTC coefficients, where each entry corresponds to the STTC coefficient between the two neurons ID-ed by the row and column indices.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sttc_mat = sttc.matrix_spike_time_tiling_coefficient(\n",
    "    binarized_traces, sample_rate=sample_session.sample_rate, dt=0.25\n",
    ")\n",
    "\n",
    "plt.imshow(sttc_mat)\n",
    "plt.colorbar()\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Once we have the cleaned-up STTC matrix, we can create a graph from it by setting a connectivity threshold, as we did before with the correlation and z-score matrices.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Get a similarity graph from the z-scores\n",
    "sg = graphs.SimilarityGraph(sttc_mat, connect_thresh=0.2)\n",
    "\n",
    "# Choose the (sub)graph to plot\n",
    "G = sg.G_nx  # Whole graph\n",
    "\n",
    "# Plot the graph\n",
    "plotting.plot_nx_graph(G)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Graphs from pre-computed STTC matrices\n",
    "\n",
    "Pre-computed STTC matrices are also stored in the session folders to speed-up data loading (see `../scripts/precompute_sttc_arrays.py` and `../slurm/precompute_sttc_arrays.sh`). The file `sttc_arrays.npz` contains a couple of arrays related to the STTC matrices just shown, and could be loaded via the method `Session.load_sttc_array_file()`. However, we've implemented some shortcuts to make the loading process easier.\n",
    "\n",
    "For instance, you may load the STTC matrix we've seen in previous cells by running:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sttc_mat = sample_session.load_sttc_array()\n",
    "plt.imshow(sttc_mat)\n",
    "plt.colorbar()\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "When creating a graph, it may be hard to set a priori a threshold for the STTC coefficients. In previous cells we've selected 0.2 as an arbitrary value, but how do we know it is a good one for each session? That is why we followed an approach similar to [Chini et al. 2023](https://www.biorxiv.org/content/10.1101/2023.11.13.566810v1): generating surrogate binarized traces, compute their STTC values, and then checking which values in the original STTC matrix are outliers in the distribution of surrogate values. For details, refer to `../notebooks/01b_choosing_sttc_thresholds.ipynb` and `../scripts/precompute_sttc_arrays.py`.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "percentiles_mat = sample_session.load_sttc_percentiles()\n",
    "plt.imshow(percentiles_mat)\n",
    "plt.colorbar()\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The percentiles matrix highlights how much of an outlier (in terms of percentile) each STTC value is in the distribution of surrogate STTC values. We can then then threshold this matrix at, say, the 95 to yield the adjacency matrix of a graph where connections are defined only by STTC value pairs that are above the 95-th percentile of the distribution of all values one could see under the random process of surrogate trace generation\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Get a similarity graph from the z-scores\n",
    "sg = graphs.SimilarityGraph(percentiles_mat, connect_thresh=95)\n",
    "\n",
    "# Choose the (sub)graph to plot\n",
    "# G = sg.G_nx  # Whole graph\n",
    "G = graphs.largest_cc(sg.G_nx)  # Largest connected component\n",
    "\n",
    "# Plot the graph\n",
    "plotting.plot_nx_graph(G)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that the minimum STTC value above the 95th-percentile in the distribution for this particular session is smaller than the 0.2 threshold we had arbitrarily set before. As a result, the graph obtained from the percentiles matrix has more connections than the one we had obtained before, albeit still being quite sparse, as connections are only established between neurons with STTC values at the very tail of the surrogate STTC distribution.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "value = np.min(np.abs(sttc_mat[percentiles_mat > 95]))\n",
    "print(\"Minimum STTC value in the 95th-percentile: {:.2f}\".format(value))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**_P.S.:_** For reproducibility, you can check the other objects stored in the `sttc_arrays.npz`. They contain, apart from the STTC and percentiles matrices, the synchronicity window size used to compute the STTC values, the number of random trials to generate surrogate STTC matrices, and the seed for the pseudo-random processes set when running `../scripts/precompute_sttc_arrays.py`:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "_, _, dt, num_trials, seed = sample_session.load_sttc_array_file()\n",
    "print(\"Synchronicity window: {} ms\".format(dt))\n",
    "print(\"Number of trials: {}\".format(num_trials))\n",
    "print(\"Random seed: {}\".format(seed))"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3.10.4 ('neural-graphs-mice-development')",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.13"
  },
  "orig_nbformat": 4,
  "vscode": {
   "interpreter": {
    "hash": "d9c38bc351936cd29f41b027944acd72081f5aecde362cbc64307b9dc28d2922"
   }
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
