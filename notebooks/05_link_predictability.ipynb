{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Link predictability comparisons\n",
    "\n",
    "Comparing how easy it is to predict links in neural activity networks of mice across different development stage windows.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import os\n",
    "import warnings\n",
    "from datetime import datetime\n",
    "\n",
    "import matplotlib.pyplot as plt\n",
    "import nb_config\n",
    "import nb_plotting\n",
    "import nb_utils\n",
    "import pandas as pd\n",
    "\n",
    "from ngmd import dataset, graphs, plotting, utils\n",
    "from ngmd import structural_consistency as sc\n",
    "\n",
    "%reload_ext autoreload\n",
    "%autoreload 2\n",
    "\n",
    "warnings.simplefilter(action=\"ignore\", category=FutureWarning)\n",
    "\n",
    "# Dataset path as a global variable. Remember to set the correct path to the\n",
    "# dataset in `notebooks/nb_config.py`\n",
    "DATA_DIR = nb_config.DATA_DIR\n",
    "\n",
    "# Models path as a global variable. Remember to set the correct path to the\n",
    "# models folder in `notebooks/nb_config.py`\n",
    "MODELS_DIR = nb_config.MODELS_DIR\n",
    "\n",
    "# Path to directory with pre-computed analysis files\n",
    "ANALYSIS_DIR = os.path.join(DATA_DIR, \"analysis_files\")\n",
    "\n",
    "# Choose a similarity computation method\n",
    "# SIMILARITY_METHOD = \"zscores\"\n",
    "SIMILARITY_METHOD = \"sttc_percentiles\"\n",
    "\n",
    "# Set a global connectivity threshold\n",
    "CONNECT_THRESH = nb_utils.select_default_connect_thresh(SIMILARITY_METHOD)\n",
    "\n",
    "# Keep results only for sessions marked as \"good\" in the dataset. Sessions that were marked as bad are those for which at least some of the analysis metrics were to far away from what one would see the days right before and after. You can still load the results for all sessions by setting the variable below to False.\n",
    "KEEP_ONLY_GOOD_SESSIONS = True\n",
    "# KEEP_ONLY_GOOD_SESSIONS = False\n",
    "\n",
    "# Load results for subsampled networks. Set this to None to load the results for\n",
    "# the full networks in each session.\n",
    "SUBSAMPLE_SIZE = 250\n",
    "# SUBSAMPLE_SIZE = None"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Get all the animal IDs in the dataset\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "animal_dataset = dataset.AnimalDataset(DATA_DIR)\n",
    "for animal in animal_dataset.animals:\n",
    "    print(animal.animal_id)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Choose an example session\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "example_id = \"DON-006084\"\n",
    "mouse_example = animal_dataset.get_animal_from_id(example_id)\n",
    "example_session = mouse_example.get_session_from_date(\"20210519\")\n",
    "print(\"Animal ID: {}\".format(example_session.animal_id))\n",
    "print(\"Session date: {}\".format(datetime.strptime(example_session.date, \"%Y%m%d\")))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Build a network from the example session's data\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sim_mat = utils.select_sim_mat(example_session, SIMILARITY_METHOD)\n",
    "example_sg = graphs.SimilarityGraph(sim_mat, connect_thresh=CONNECT_THRESH)\n",
    "\n",
    "# Choose which (sub)graph to plot\n",
    "largest_cc_only = True  # Plot only the largest connected component\n",
    "if largest_cc_only:\n",
    "    G = graphs.largest_cc(example_sg.G_nx)  # Largest connected component\n",
    "else:\n",
    "    G = example_sg.G_nx  # The whole graph\n",
    "\n",
    "# Plot the graph\n",
    "ax = plotting.plot_nx_graph(G)\n",
    "title = \"{} at PDay {}\".format(example_id, example_session.dev_stage)\n",
    "if largest_cc_only:\n",
    "    title += \" (largest CC only)\"\n",
    "ax.set_title(title)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## How predictable are the links in a network? The structural consistency perspective\n",
    "\n",
    "[(Lü et al., 2015)](https://pnas.org/doi/full/10.1073/pnas.1424644112) define structural consistency as a way to estimate how predictable the links in a network are. The idea is that links are harder to predict if their removal causes large structural changes in the network. The structural consistency metric is derived by removing links from the network uniformly at random, then trying to approximate the network's original adjacency matrix using the eigenvalues and eigenvectors of the perturbed adjacency matrix. If the approximation is good, in a well-defined sense, then the network has high structural consistency. This value is normalized, so that the lowest possible structural consistency is 0, and the highest possible is 1.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Erdös-Rényi (ER) network have low structural consistency, due to their random nature. Let us draw an ER network with 100 nodes and 0.1 link probability:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import networkx as nx\n",
    "\n",
    "G_nx = nx.erdos_renyi_graph(100, 0.1)\n",
    "plotting.plot_nx_graph(G_nx)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The structural consistency of ER(100, 0.1) should be close t 0.15. There are variations due to the random nature of the network and the random process defining the structural consistency metric.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sigma = sc.structural_consistency(G_nx, p_selection=0.1)\n",
    "print(f\"Structural consistency: {sigma:.4f}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This value gets only lower as we increase the number of nodes, meaning the links in the network get harder to predict.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Draw an ER graph with 1000 nodes and p=0.1\n",
    "G_nx = nx.erdos_renyi_graph(1000, 0.1)\n",
    "\n",
    "sigma = sc.structural_consistency(G_nx, p_selection=0.1)\n",
    "print(f\"Structural consistency: {sigma:.4f}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Other classical random graphs are easier to predict. For example, the Watts-Strogatz (WS) model, which exhibits small-world characteristics, has higher structural consistency. Let us draw a WS network with 1000 nodes, average degree 6, and 0.1 rewiring probability:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "G_nx = nx.watts_strogatz_graph(1000, 6, 0.1)\n",
    "plotting.plot_nx_graph(G_nx)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The structural consistency of WS(1000, 6, 0.1) should be close to 0.35 (once again, barring random variations).\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# According to Lü et al. (2015), the structural consistency of WS(1000, 6, 0.1) should be close to 0.35\n",
    "sigma = sc.structural_consistency(G_nx, p_selection=0.1)\n",
    "print(f\"Structural consistency: {sigma:.4f}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "What is the structural consistency of the example graph from our dataset? Run the cell below a couple of times to get a sense of the variation\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sigma = sc.structural_consistency(\n",
    "    example_sg.G_nx,\n",
    "    p_selection=0.1,\n",
    ")\n",
    "print(f\"Structural consistency: {sigma:.4f}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "With aid from the script `../scripts/structural_consistency.py`, we computed the structural consistency of all the networks in the dataset. To tame random variations, we computed the structural consistency of each network 20 times and recorded the average, standard deviation, 5th and 95th percentiles in that distribution. Here is how these results look like on a table.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Load the pre-computed structural consistency results\n",
    "file_name = \"structural_consistency_{}\".format(SIMILARITY_METHOD)\n",
    "file_name += \"_connect_thresh={:.1f}\".format(CONNECT_THRESH)\n",
    "if SUBSAMPLE_SIZE is not None:\n",
    "    file_name += \"_subsample_size={}\".format(SUBSAMPLE_SIZE)\n",
    "file_name += \".csv\"\n",
    "sc_df = pd.read_csv(os.path.join(ANALYSIS_DIR, file_name))\n",
    "\n",
    "# Keep only the rows corresponding to \"good\" sessions\n",
    "if KEEP_ONLY_GOOD_SESSIONS:\n",
    "    sc_df = nb_utils.keep_rows_from_good_sessions(sc_df, animal_dataset)\n",
    "\n",
    "sc_df.head()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can plot the structural consistency values as a function of developmental days:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Sort the dataframe by animal ID and dev_stage\n",
    "sc_df = sc_df.sort_values(by=[\"animal_id\", \"dev_stage\"])\n",
    "\n",
    "fig = nb_plotting.line_with_interval(\n",
    "    data_frame=sc_df,\n",
    "    x=\"dev_stage\",\n",
    "    y=\"mean_sc\",\n",
    "    error_y_mode=\"bands\",\n",
    "    error_y=\"std_sc\",\n",
    "    title=\"Structural consistency throughout development\",\n",
    "    color=\"animal_id\",\n",
    ")\n",
    "\n",
    "fig.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# We can use the line below to update the table of bad sessions after looking at this\n",
    "# day-by-day plot. The idea is to identify sessions whose metric is too far away from\n",
    "# what was observe the days right before and after.\n",
    "\n",
    "# nb_utils.update_bad_sessions(sc_df, \"DON-015079\", 24, animal_dataset)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that some animals display huge variation in the recorded structural consistency from day to day. To try to smooth out this variation, we can sort the developmental days into bins:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Use the default development stage cuts\n",
    "dev_stage_cuts = utils.get_default_dev_stage_cuts()\n",
    "\n",
    "print(\"The selected development windows are:\")\n",
    "print(\"\\n\".join([label for label in nb_utils.labels_from_cuts(dev_stage_cuts)]))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sc_dev_grouping = nb_utils.group_by_dev_interval(\n",
    "    sc_df,\n",
    "    dev_stage_cuts,\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And now, compile the distribution of structural consistency values across all animals in each single bin:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dev_window_values = []\n",
    "dev_window_labels = []\n",
    "for name, df in sc_dev_grouping:\n",
    "    dev_window_values.append(df[\"mean_sc\"].values)\n",
    "    dev_window_labels.append(name)\n",
    "\n",
    "fig, ax = plt.subplots(figsize=(10, 5))\n",
    "ax.boxplot(\n",
    "    dev_window_values,\n",
    "    bootstrap=5000,\n",
    "    notch=True,\n",
    "    showfliers=False,\n",
    ")\n",
    "ax.set_xticklabels(dev_window_labels)\n",
    "ax.set_ylabel(\"Structural consistency\")\n",
    "ax.set_xlabel(\"Developmental window\")\n",
    "ax.set_title(\"Structural consistency by developmental stage\")\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "neural-graphs-mice-development",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.13"
  },
  "vscode": {
   "interpreter": {
    "hash": "d9c38bc351936cd29f41b027944acd72081f5aecde362cbc64307b9dc28d2922"
   }
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
