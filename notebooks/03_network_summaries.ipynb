{
 "cells": [
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Network summary comparisons\n",
    "\n",
    "Comparisons between summaries of neural activity networks in infant and adult mice.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import os\n",
    "import warnings\n",
    "\n",
    "import matplotlib.pyplot as plt\n",
    "import nb_config\n",
    "import nb_plotting\n",
    "import nb_utils\n",
    "import numpy as np\n",
    "import pandas as pd\n",
    "import plotly.express as px\n",
    "\n",
    "from ngmd import dataset, permutation_tests, plotting, utils\n",
    "\n",
    "%reload_ext autoreload\n",
    "%autoreload 2\n",
    "\n",
    "warnings.simplefilter(action=\"ignore\", category=FutureWarning)\n",
    "\n",
    "# Dataset path as a global variable. Remember to set the correct path to the\n",
    "# dataset in `notebooks/nb_config.py`\n",
    "DATA_DIR = nb_config.DATA_DIR\n",
    "\n",
    "# Choose a similarity computation method\n",
    "SIMILARITY_METHOD = \"sttc_percentiles\"\n",
    "# SIMILARITY_METHOD = \"zscores\"\n",
    "\n",
    "# Set a connectivity threshold to be investigated in more detail\n",
    "CONNECT_THRESH = nb_utils.select_default_connect_thresh(SIMILARITY_METHOD)\n",
    "\n",
    "# Keep results only for sessions marked as \"good\" in the dataset. Sessions that were marked as bad are those for which at least some of the analysis metrics were to far away from what one would see the days right before and after. You can still load the results for all sessions by setting the variable below to False.\n",
    "KEEP_ONLY_GOOD_SESSIONS = True\n",
    "# KEEP_ONLY_GOOD_SESSIONS = False\n",
    "\n",
    "# Load results for subsampled networks. Set this to None to load the results for\n",
    "# the full networks in each session.\n",
    "SUBSAMPLE_SIZE = 250\n",
    "# SUBSAMPLE_SIZE = None"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Loading the animal dataset\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "animal_dataset = dataset.AnimalDataset(DATA_DIR)"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Scalar summaries of network properties\n",
    "\n",
    "Each experiment session can be linked to a neural activity graphs built from the measured calcium fluorescence data. The first thing we may do to quantify properties of these graphs is to compile some standard network science summaries. Here are the summaries we will record.\n",
    "\n",
    "- **Number of connected components**.\n",
    "- **Density**: fraction of all possible edges.\n",
    "- **Transitivity**: fraction of all possible triangles.\n",
    "- [**Average clustering coefficient**](https://en.wikipedia.org/wiki/Clustering_coefficient).\n",
    "  - The local clustering coefficient of a node quantifies how close it and its neighbors are to being a clique. Small-world networks have high average clustering coefficients\n",
    "- **Size of the largest connected component**.\n",
    "  - Normalized by number of nodes\n",
    "- **Average shortest path length** of the largest connected component.\n",
    "- **Small-worldness** of the largest connected component.\n",
    "  - Small-world coefficient from Telesford et al. (2011).\n",
    "  - To compute the coefficient, one first compares the average clustering coefficient of the network to the same summary of an equivalent lattice network, then compares the average shortest path length of the network to the same summary of an equivalent random network. The small-world coefficient is then the difference of ratios issues from these two comparisons.\n",
    "  - This coefficient should range between -1 and 1. Values close to zero indicate graphs with more small-world characteristics. Positive values indicate a graph with more random characteristics. Negative values indicate a graph with more regular, or lattice-like, characteristics.\n",
    "- **Radius** and **Diameter** of the largest connected component (a.k.a. minimum and maximum eccentricity, respectively).\n",
    "  - The eccentricity of a node is the maximum distance from this node to all other nodes in the graph.\n",
    "- **Median Page Rank**\n",
    "  - The page rank of a node measures the node's \"importance\" in the network via the number of edges that connect to it. Quality of the connections also counts: if a node receives connections from a node with large page rank, then the former node will tend to have large page rank as well. The median of the page ranks in the network is the summary we record for each neural activity graph. Being a quantile, we know that the higher the median, the more nodes are considered important.\n",
    "\n",
    "I have pre-computed the network summaries above for some connectivity thresholds. Let us load these summaries and compare how these summaries change as the connectivity threshold increases. We should expect quantities like the number of connected components to decrease, whereas the density should decrease. But these changes need not happen at the same rate for different groups of mice. We will present the summaries reduced by development stage groups, because we would like to catch distinctions across networks of infant and adult mice, not necessarily across individual mice.\n",
    "\n",
    "_Remarks_:\n",
    "\n",
    "- You may recompute these summaries for a given connectivity threshold by yourself using the script `scripts/network_summaries.py`.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "thresholds = [90, 95, 96, 97, 98, 98, 99]\n",
    "\n",
    "dfs = []\n",
    "for t in thresholds:\n",
    "    file_name = \"network_summaries_{}\".format(SIMILARITY_METHOD)\n",
    "    file_name += \"_connect_thresh={:.1f}\".format(t)\n",
    "    if SUBSAMPLE_SIZE is not None:\n",
    "        file_name += \"_subsample_size={}\".format(SUBSAMPLE_SIZE)\n",
    "    file_name += \".csv\"\n",
    "    summary_path = os.path.join(DATA_DIR, \"analysis_files\", file_name)\n",
    "    if os.path.exists(summary_path):\n",
    "        df = pd.read_csv(summary_path)\n",
    "        df[\"connect_thresh\"] = t\n",
    "\n",
    "        # Small-worldness is in a separate file\n",
    "        sm_path = os.path.join(\n",
    "            DATA_DIR,\n",
    "            \"analysis_files\",\n",
    "            \"small_worldness_{}_connect_thresh={:.1f}\".format(SIMILARITY_METHOD, t)\n",
    "            + \".csv\",\n",
    "        )\n",
    "        if os.path.exists(sm_path):\n",
    "            sm_df = pd.read_csv(sm_path)\n",
    "            df[\"small_worldness\"] = sm_df[\"small_worldness\"]\n",
    "        else:\n",
    "            df[\"small_worldness\"] = np.nan\n",
    "\n",
    "        # Sort animal_id and dev_stage, in ascending order\n",
    "        df = df.sort_values([\"animal_id\", \"dev_stage\"])\n",
    "        dfs.append(df)\n",
    "\n",
    "summaries_df = pd.concat(dfs, ignore_index=True)\n",
    "summaries_df.head()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Discard rows corresponding to \"bad\" sessions. These are sessions that were marked as bad during the analysis because their metric was to far away from what one would see the days right before and after. If you comment out the line restricting the data frame only to the good sessions, you'll see some large, unexpected spikes in the day-by-day values plotted in some of the cells below. These are the bad sessions.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "if KEEP_ONLY_GOOD_SESSIONS:\n",
    "    # Keep only the rows corresponding to \"good\" sessions\n",
    "    summaries_df = nb_utils.keep_rows_from_good_sessions(summaries_df, animal_dataset)"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The development stage windows are defined by quantiles, so that each of the 5 windows have the same number of representatives. You may also define arbitrary windows by specifying the window borders (cuts).\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Use default developmental stage windows\n",
    "dev_stage_cuts = utils.get_default_dev_stage_cuts()\n",
    "\n",
    "print(\"The selected development windows are:\")\n",
    "print(\"\\n\".join([label for label in nb_utils.labels_from_cuts(dev_stage_cuts)]))"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Each network summary table, for each connectivity threshold is recorded as a single dictionary for easy access. For example, `num_neurons[5]['PDay 15-22']` contains the number of neurons for networks in the development window 'PDay 15-22' at connectivity threshold `5`.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "num_neurons = {}\n",
    "num_conn_comp = {}\n",
    "density = {}\n",
    "transitivity = {}\n",
    "avg_clust = {}\n",
    "size_largest_cc = {}\n",
    "avg_short_path_length = {}\n",
    "radius = {}\n",
    "diameter = {}\n",
    "median_page_rank = {}\n",
    "small_worldness = {}\n",
    "\n",
    "connect_thresh_grouping = summaries_df.groupby(summaries_df[\"connect_thresh\"])\n",
    "\n",
    "for key, df in connect_thresh_grouping:\n",
    "    dev_grouping = nb_utils.group_by_dev_interval(df, dev_stage_cuts)\n",
    "\n",
    "    num_neurons[key] = {}\n",
    "    num_conn_comp[key] = {}\n",
    "    density[key] = {}\n",
    "    transitivity[key] = {}\n",
    "    avg_clust[key] = {}\n",
    "    size_largest_cc[key] = {}\n",
    "    avg_short_path_length[key] = {}\n",
    "    radius[key] = {}\n",
    "    diameter[key] = {}\n",
    "    median_page_rank[key] = {}\n",
    "    small_worldness[key] = {}\n",
    "\n",
    "    for dev_key, dev_df in dev_grouping:\n",
    "        num_neurons[key][dev_key] = dev_df[\"num_neurons\"].values\n",
    "        num_conn_comp[key][dev_key] = dev_df[\"num_conn_comp\"].values\n",
    "        density[key][dev_key] = dev_df[\"density\"].values\n",
    "        transitivity[key][dev_key] = dev_df[\"transitivity\"].values\n",
    "        avg_clust[key][dev_key] = dev_df[\"avg_clust\"].values\n",
    "        size_largest_cc[key][dev_key] = (\n",
    "            dev_df[\"size_largest_cc\"].values / dev_df[\"num_neurons\"].values\n",
    "        )\n",
    "        avg_short_path_length[key][dev_key] = dev_df[\"avg_short_path_length\"].values\n",
    "        radius[key][dev_key] = dev_df[\"radius\"].values\n",
    "        diameter[key][dev_key] = dev_df[\"diameter\"].values\n",
    "        median_page_rank[key][dev_key] = dev_df[\"median_page_rank\"].values\n",
    "        small_worldness[key][dev_key] = dev_df[\"small_worldness\"].values"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Number of neurons\n",
    "\n",
    "The first of the the summaries, the number of neurons, does not change as we change the connectivity threshold. Hence, we can pick an arbitrary threshold value to display this summary. As we can see, there is some variability in the number of neurons identified in each experimental session of each developmental window. These numbers vary (roughly) between below 250 to above 1000, discounting outliers. Despite the variation, the distributions are very similar across the developmental windows.\n",
    "\n",
    "On the one hand, the number of identified neurons does not differentiate development windows. On the other hand, we do not need to worry about biases in this regard when comparing different windows.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ax = nb_plotting.boxplot_summary({\"NA\": num_neurons[CONNECT_THRESH]})\n",
    "ax.set_title(\"\")\n",
    "plt.show()"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here's a view of how the number of neurons varies per animal throughout development.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "summaries_df_connect_thresh = connect_thresh_grouping.get_group(CONNECT_THRESH).copy(\n",
    "    deep=True\n",
    ")\n",
    "fig = px.line(\n",
    "    summaries_df_connect_thresh, x=\"dev_stage\", y=\"num_neurons\", color=\"animal_id\"\n",
    ")\n",
    "fig.show()"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Number of connected components\n",
    "\n",
    "The plot below shows the distributions of the number of connected components within each of the previously development windows. Each subplot corresponds to a choice of connectivity threshold when creating the graphs from the correlation matrices. When the threshold increases, we see some of what we already expected: the number of connected components tends to increase as well. After all, we are progressively removing \"weak\" edges from the graphs, which tends to divide the later into more and more connected components. However, the rate at which the number of connected components increases is not necessarily the same for each developmental stage window.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ax = nb_plotting.boxplot_summary(num_conn_comp, showfliers=False)\n",
    "plt.show()"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let us focus now on `connect_thresh==CONNECT_THRESH` to examine in more detail how the distributions of this particular summary vary across development windows.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ax = nb_plotting.boxplot_summary(\n",
    "    {\"NA\": num_conn_comp[CONNECT_THRESH]}, showfliers=False\n",
    ")\n",
    "plt.gcf().set_size_inches(8, 4)\n",
    "ax.set_title(\"connect_thresh={:.1f}\".format(CONNECT_THRESH))\n",
    "plt.show()"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As we've done with the number of neurons before, we can look at how the number of connected components varies per animal (rather than the summary distributions per developmental window above).\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig = px.line(\n",
    "    summaries_df_connect_thresh, x=\"dev_stage\", y=\"num_conn_comp\", color=\"animal_id\"\n",
    ")\n",
    "fig.show()"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can assess the differences in distributions by running pairwise statistical tests between the development windows.\n",
    "\n",
    "The test statistics chosen here is the difference-of-means between the distributions of the pair of development windows being tested. The null hypothesis is that the two sets of summaries comes from the same distribution, corresponding to a test statistic equal or close to zero.\n",
    "\n",
    "To measure how extreme the test statistic between two development windows is, we run a [permutation test](https://en.wikipedia.org/wiki/Permutation_test). The p-value resulting from such test corresponds to the probability of observing a statistic as extreme as the measured one, according to the permutation distribution. In terms of the hypothesis being tested, the p-value should be interpreted as the evidence for an actual difference between the distributions, as provided by the data.\n",
    "\n",
    "Since we are testing for differences across all pairs of development windows, we perform multiple hypothesis tests in the end. To correct for multiple testing, we then adjust the individual p-values using a Holm step down method using Sidak adjustments. We assume a family-wise-error-rate (FWER) of 0.05 when deciding to reject the null-hypothesis.\n",
    "\n",
    "In the end, we can obtain a plot like the one below, which can be read as follows. Each cell represents a test of its corresponding row versus its corresponding column. A white cell indicates that we can't conclude that there is a significant difference (i.e. the null hypothesis is accepted at FWER = 0.05). Colors indicate statistically significant differences (at the same FWER) and their direction. Green is for positive, pink for negative. Darker shades of green or pink indicate the magnitude of the difference.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ax, results = permutation_tests.plot_stat_diff(\n",
    "    num_conn_comp[CONNECT_THRESH],\n",
    "    return_all=True,\n",
    ")\n",
    "ax.set_title(\"Number of connected components\")\n",
    "plt.show()"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can look at what are the actual (multiple-test-corrected) p-values and statistics of the tests by examining the `results` dictionary. The table below shows which pairs of development windows passed the permutation test, along with their corresponding test statistics and p-values.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "nb_utils.format_table_of_passed_tests(results)"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**_Remarks:_**\n",
    "\n",
    "- This session on the number of connected components is the only one among the summary sessions where we explain what happens in each cell. All the subsequent notebook sessions follow the same narrative, so only the plots are shown. Commentary resumes in the end, with some conclusions.\n"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Size of the largest connected component\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ax = nb_plotting.boxplot_summary(size_largest_cc)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ax = nb_plotting.boxplot_summary(\n",
    "    {\"NA\": size_largest_cc[CONNECT_THRESH]}, showfliers=False\n",
    ")\n",
    "plt.gcf().set_size_inches(8, 4)\n",
    "ax.set_title(\"connect_thresh={:.1f}\".format(CONNECT_THRESH))\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "summaries_df_connect_thresh[\"rel_size_largest_cc\"] = (\n",
    "    summaries_df_connect_thresh[\"size_largest_cc\"]\n",
    "    / summaries_df_connect_thresh[\"num_neurons\"]\n",
    ")\n",
    "fig = px.line(\n",
    "    summaries_df_connect_thresh,\n",
    "    x=\"dev_stage\",\n",
    "    y=\"rel_size_largest_cc\",\n",
    "    color=\"animal_id\",\n",
    ")\n",
    "fig.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ax, results = permutation_tests.plot_stat_diff(\n",
    "    size_largest_cc[CONNECT_THRESH],\n",
    "    return_all=True,\n",
    ")\n",
    "ax.set_title(\"(Relative) size of the largest c.c.\")\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "nb_utils.format_table_of_passed_tests(results)"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Density\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ax = nb_plotting.boxplot_summary(density, showfliers=False)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ax = nb_plotting.boxplot_summary({\"NA\": density[CONNECT_THRESH]}, showfliers=False)\n",
    "plt.gcf().set_size_inches(8, 4)\n",
    "ax.set_title(\"connect_thresh={:.1f}\".format(CONNECT_THRESH))\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig = px.line(\n",
    "    summaries_df_connect_thresh, x=\"dev_stage\", y=\"density\", color=\"animal_id\"\n",
    ")\n",
    "fig.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ax, results = permutation_tests.plot_stat_diff(\n",
    "    density[CONNECT_THRESH],\n",
    "    return_all=True,\n",
    ")\n",
    "ax.set_title(\"Edge density\")\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "nb_utils.format_table_of_passed_tests(results)"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Transitivity\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ax = nb_plotting.boxplot_summary(transitivity)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ax = nb_plotting.boxplot_summary({\"NA\": transitivity[CONNECT_THRESH]}, showfliers=False)\n",
    "plt.gcf().set_size_inches(8, 4)\n",
    "ax.set_title(\"connect_thresh={:.1f}\".format(CONNECT_THRESH))\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig = px.line(\n",
    "    summaries_df_connect_thresh, x=\"dev_stage\", y=\"transitivity\", color=\"animal_id\"\n",
    ")\n",
    "fig.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ax, results = permutation_tests.plot_stat_diff(\n",
    "    transitivity[CONNECT_THRESH],\n",
    "    return_all=True,\n",
    ")\n",
    "ax.set_title(\"Transitivity\")\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "nb_utils.format_table_of_passed_tests(results)"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Average clustering\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ax = nb_plotting.boxplot_summary(avg_clust)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ax = nb_plotting.boxplot_summary({\"NA\": avg_clust[CONNECT_THRESH]}, showfliers=False)\n",
    "plt.gcf().set_size_inches(8, 4)\n",
    "ax.set_title(\"connect_thresh={:.1f}\".format(CONNECT_THRESH))\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig = px.line(\n",
    "    summaries_df_connect_thresh, x=\"dev_stage\", y=\"avg_clust\", color=\"animal_id\"\n",
    ")\n",
    "fig.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ax, results = permutation_tests.plot_stat_diff(\n",
    "    avg_clust[CONNECT_THRESH],\n",
    "    return_all=True,\n",
    ")\n",
    "ax.set_title(\"Average clustering\")\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "nb_utils.format_table_of_passed_tests(results)"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Average shortest path length (of the largest connected component)\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ax = nb_plotting.boxplot_summary(avg_short_path_length)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ax = nb_plotting.boxplot_summary(\n",
    "    {\"NA\": avg_short_path_length[CONNECT_THRESH]}, showfliers=False\n",
    ")\n",
    "plt.gcf().set_size_inches(8, 4)\n",
    "ax.set_title(\"connect_thresh={:.1f}\".format(CONNECT_THRESH))\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig = px.line(\n",
    "    summaries_df_connect_thresh,\n",
    "    x=\"dev_stage\",\n",
    "    y=\"avg_short_path_length\",\n",
    "    color=\"animal_id\",\n",
    ")\n",
    "fig.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ax, results = permutation_tests.plot_stat_diff(\n",
    "    avg_short_path_length[CONNECT_THRESH],\n",
    "    return_all=True,\n",
    ")\n",
    "ax.set_title(\"Average shortest path length\")\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "nb_utils.format_table_of_passed_tests(results)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Small-worldness coefficient (of the largest connected component)\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ax = nb_plotting.boxplot_summary(small_worldness)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ax = nb_plotting.boxplot_summary(\n",
    "    {\"NA\": small_worldness[CONNECT_THRESH]}, showfliers=False\n",
    ")\n",
    "plt.gcf().set_size_inches(18, 4)\n",
    "ax.set_title(\"connect_thresh={:.1f}\".format(CONNECT_THRESH))\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig = px.line(\n",
    "    summaries_df_connect_thresh, x=\"dev_stage\", y=\"small_worldness\", color=\"animal_id\"\n",
    ")\n",
    "fig.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# # Keep only non-NaN values\n",
    "invalid = False\n",
    "values = small_worldness[CONNECT_THRESH]\n",
    "for key in values:\n",
    "    values[key] = values[key][~np.isnan(values[key])]\n",
    "    if len(values[key]) == 0:\n",
    "        invalid = True\n",
    "\n",
    "if not invalid:\n",
    "    ax, results = permutation_tests.plot_stat_diff(\n",
    "        values,\n",
    "        return_all=True,\n",
    "    )\n",
    "    ax.set_title(\"Small-worldness\")\n",
    "    plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "if not invalid:\n",
    "    nb_utils.format_table_of_passed_tests(results)"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Radius (of the largest connected component)\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ax = nb_plotting.boxplot_summary(radius)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ax = nb_plotting.boxplot_summary({\"NA\": radius[CONNECT_THRESH]}, showfliers=False)\n",
    "plt.gcf().set_size_inches(8, 4)\n",
    "ax.set_title(\"connect_thresh={:.1f}\".format(CONNECT_THRESH))\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig = px.line(summaries_df_connect_thresh, x=\"dev_stage\", y=\"radius\", color=\"animal_id\")\n",
    "fig.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ax, results = permutation_tests.plot_stat_diff(\n",
    "    radius[CONNECT_THRESH],\n",
    "    return_all=True,\n",
    ")\n",
    "ax.set_title(\"Radius\")\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "nb_utils.format_table_of_passed_tests(results)"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Diameter (of the largest connected component)\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ax = nb_plotting.boxplot_summary(diameter)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ax = nb_plotting.boxplot_summary({\"NA\": diameter[CONNECT_THRESH]}, showfliers=False)\n",
    "plt.gcf().set_size_inches(8, 4)\n",
    "ax.set_title(\"connect_thresh={:.1f}\".format(CONNECT_THRESH))\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig = px.line(\n",
    "    summaries_df_connect_thresh, x=\"dev_stage\", y=\"diameter\", color=\"animal_id\"\n",
    ")\n",
    "fig.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ax, results = permutation_tests.plot_stat_diff(\n",
    "    diameter[CONNECT_THRESH],\n",
    "    return_all=True,\n",
    ")\n",
    "ax.set_title(\"Diameter\")\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "nb_utils.format_table_of_passed_tests(results)"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Median page rank\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ax = nb_plotting.boxplot_summary(median_page_rank)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ax = nb_plotting.boxplot_summary(\n",
    "    {\"NA\": median_page_rank[CONNECT_THRESH]}, showfliers=False\n",
    ")\n",
    "plt.gcf().set_size_inches(8, 4)\n",
    "ax.set_title(\"connect_thresh={:.1f}\".format(CONNECT_THRESH))\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig = px.line(\n",
    "    summaries_df_connect_thresh, x=\"dev_stage\", y=\"median_page_rank\", color=\"animal_id\"\n",
    ")\n",
    "fig.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ax, results = permutation_tests.plot_stat_diff(\n",
    "    median_page_rank[CONNECT_THRESH],\n",
    "    return_all=True,\n",
    ")\n",
    "ax.set_title(\"Median Page Rank\")\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "nb_utils.format_table_of_passed_tests(results)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Illustrating developmental windows and properties that stand out\n",
    "\n",
    "In this section we will illustrate graphs typical of the network summaries that stand out. Pairs of developmental windows are chosen based on largest effect size between the summary distributions of windows being compared.\n",
    "\n",
    "Let us create our dataset object to load data from:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "animal_dataset = dataset.AnimalDataset(DATA_DIR)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And restrict the summaries table only to the connection threshold of interest:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Restrict summaries to the connectivity threshold of interest\n",
    "connect_thresh_grouping = summaries_df.groupby(\"connect_thresh\")\n",
    "restricted_df = connect_thresh_grouping.get_group(CONNECT_THRESH)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Choose a property and two developmental windows to compare\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "property = \"transitivity\"\n",
    "dev_window_1 = [90, 300]\n",
    "dev_window_2 = [15, 24]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "typical_samples = nb_utils.get_typical_samples(\n",
    "    restricted_df,\n",
    "    summary_name=property,\n",
    "    dev_window=dev_window_1,\n",
    ")\n",
    "typical_networks = nb_utils.build_networks(\n",
    "    animal_dataset,\n",
    "    typical_samples,\n",
    "    sim_type=SIMILARITY_METHOD,\n",
    "    connect_thresh=CONNECT_THRESH,\n",
    ")\n",
    "fig, axs = plotting.networks_side_by_side(typical_networks, min_cc_size=10)\n",
    "fig.suptitle(\n",
    "    \"Typical networks for the {} property in dev. window {}\".format(\n",
    "        property, dev_window_1\n",
    "    ),\n",
    "    fontsize=16,\n",
    ")\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "typical_samples = nb_utils.get_typical_samples(\n",
    "    restricted_df,\n",
    "    summary_name=property,\n",
    "    dev_window=dev_window_2,\n",
    ")\n",
    "typical_networks = nb_utils.build_networks(\n",
    "    animal_dataset,\n",
    "    typical_samples,\n",
    "    sim_type=SIMILARITY_METHOD,\n",
    "    connect_thresh=CONNECT_THRESH,\n",
    ")\n",
    "fig, axs = plotting.networks_side_by_side(typical_networks, min_cc_size=10)\n",
    "fig.suptitle(\n",
    "    \"Typical networks for the {} property in dev. window {}\".format(\n",
    "        property, dev_window_2\n",
    "    ),\n",
    "    fontsize=16,\n",
    ")\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "neural-graphs-mice-development",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.13"
  },
  "vscode": {
   "interpreter": {
    "hash": "d9c38bc351936cd29f41b027944acd72081f5aecde362cbc64307b9dc28d2922"
   }
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
