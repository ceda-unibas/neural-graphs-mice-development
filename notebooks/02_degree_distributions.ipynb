{
 "cells": [
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Degree comparisons\n",
    "\n",
    "Comparisons between degree distributions of neural activity networks in infant and adult mice.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import os\n",
    "\n",
    "import matplotlib.pyplot as plt\n",
    "import nb_config\n",
    "import nb_plotting\n",
    "import nb_utils\n",
    "import networkx as nx\n",
    "import numpy as np\n",
    "import pandas as pd\n",
    "from matplotlib import colormaps\n",
    "\n",
    "from ngmd import (\n",
    "    dataset,\n",
    "    graphs,\n",
    "    plotting,\n",
    "    utils,\n",
    ")\n",
    "\n",
    "%load_ext autoreload\n",
    "%autoreload 2\n",
    "\n",
    "# Dataset path as a global variable. Remember to set the correct path to the\n",
    "# dataset in `notebooks/nb_config.py`\n",
    "DATA_DIR = nb_config.DATA_DIR\n",
    "\n",
    "# Path to directory with pre-computed analysis files\n",
    "ANALYSIS_DIR = os.path.join(DATA_DIR, \"analysis_files\")\n",
    "\n",
    "# Color palette for the plots\n",
    "PALETTE = plotting.UnibasColors()\n",
    "\n",
    "# Choose a similarity computation method\n",
    "# SIMILARITY_METHOD = \"zscores\"\n",
    "SIMILARITY_METHOD = \"sttc_percentiles\"\n",
    "\n",
    "# Set a global connectivity threshold\n",
    "CONNECT_THRESH = nb_utils.select_default_connect_thresh(SIMILARITY_METHOD)"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Loading all animal data\n"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here are all the animal folders currently available in the dataset.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "animal_dataset = dataset.AnimalDataset(DATA_DIR)\n",
    "for animal in animal_dataset.animals:\n",
    "    print(animal.animal_id)"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Degree distributions\n"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A common way to quantify properties of a network is to compile its degree distribution. We mean \"degree\" in a general sense in this section, that is, the degree of each node is the sum of the weights of its connections. Since weights are correlation values in the neural activity graphs, this means that a degree is a measure of how correlated a neuron in with other neurons in the network.\n",
    "\n",
    "We will pick two example sessions/networks to use for illustrative purposes throughout the rest of this notebook. One of them represents a young mouse, the other an adult.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "young_mouse_example = animal_dataset.get_animal_from_id(\"DON-006087\")\n",
    "young_mouse_example_session = young_mouse_example.get_session_from_date(\"20210525\")\n",
    "young_mouse_example_sg = graphs.SimilarityGraph(\n",
    "    similarity_matrix=utils.select_sim_mat(\n",
    "        young_mouse_example_session, SIMILARITY_METHOD\n",
    "    ),\n",
    "    connect_thresh=CONNECT_THRESH,\n",
    ")\n",
    "\n",
    "old_mouse_example = animal_dataset.get_animal_from_id(\"DON-002865\")\n",
    "old_mouse_example_session = old_mouse_example.get_session_from_date(\"20210303\")\n",
    "old_mouse_example_sg = graphs.SimilarityGraph(\n",
    "    similarity_matrix=utils.select_sim_mat(\n",
    "        old_mouse_example_session, SIMILARITY_METHOD\n",
    "    ),\n",
    "    connect_thresh=CONNECT_THRESH,\n",
    ")\n",
    "\n",
    "print(\"Young mouse example session :\")\n",
    "print(\"\\t ID: {}\".format(young_mouse_example_session.animal_id))\n",
    "print(\"\\t Date: {}\".format(young_mouse_example_session.date))\n",
    "print(\"\\t Development stage: {}\".format(young_mouse_example_session.dev_stage))\n",
    "\n",
    "print(\"Old mouse example session :\")\n",
    "print(\"\\t ID: {}\".format(old_mouse_example_session.animal_id))\n",
    "print(\"\\t Date: {}\".format(old_mouse_example_session.date))\n",
    "print(\"\\t Development stage: {}\".format(old_mouse_example_session.dev_stage))"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The degree distribution is represented by a histogram. First, traverse all the nodes in the network, compiling a list of degree values. Then define a set of bins (intervals) in the real line and count how many of the degrees fall into bin 1, bin 2, ..., and so on.\n",
    "\n",
    "Here's the degree distribution of the young network example.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "young_mouse_example_degrees = [\n",
    "    float(d) for (_, d) in young_mouse_example_sg.G_nx.degree()\n",
    "]\n",
    "old_mouse_example_degrees = [float(d) for (_, d) in old_mouse_example_sg.G_nx.degree()]\n",
    "\n",
    "# Define a fixed range and number of bins for the histogram of both examples\n",
    "hist_range = (\n",
    "    np.min([np.min(young_mouse_example_degrees), np.min(old_mouse_example_degrees)]),\n",
    "    np.max([np.max(young_mouse_example_degrees), np.max(old_mouse_example_degrees)]),\n",
    ")\n",
    "n_bins = 51\n",
    "\n",
    "ax, _, _ = nb_plotting.plot_histogram(\n",
    "    young_mouse_example_degrees,\n",
    "    range=hist_range,\n",
    "    bins=n_bins,\n",
    "    histtype=\"step\",\n",
    ")\n",
    "ax.set(xlabel=\"Degree (from {})\".format(SIMILARITY_METHOD), title=\"Young mouse example\")\n",
    "ax = nb_plotting.draw_median_line(young_mouse_example_degrees, ax)\n",
    "\n",
    "plt.show()"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And here's the degree distribution of the adult network example.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ax, _, _ = nb_plotting.plot_histogram(\n",
    "    old_mouse_example_degrees,\n",
    "    range=hist_range,\n",
    "    bins=n_bins,\n",
    "    histtype=\"step\",\n",
    ")\n",
    "ax.set(xlabel=\"Degree (from {})\".format(SIMILARITY_METHOD), title=\"Old mouse example\")\n",
    "ax = nb_plotting.draw_median_line(old_mouse_example_degrees, ax)\n",
    "\n",
    "plt.show()"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To see if the behavior above repeats across the whole dataset, we can do a parallel plot of the aggregated degree distributions for each developmental window of interest. Those values were precomputed beforehand with the script `scripts/degree_distributions.py`.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Get name of the file to load from the global variables\n",
    "name = \"degree_distributions_{}_connect_thresh={:.1f}\".format(\n",
    "    SIMILARITY_METHOD, CONNECT_THRESH\n",
    ")\n",
    "# Build the path to the file\n",
    "summary_path = os.path.join(ANALYSIS_DIR, name + \".csv\")\n",
    "\n",
    "# Load the file if it exists\n",
    "if os.path.exists(summary_path):\n",
    "    degrees_df = pd.read_csv(summary_path, converters={\"degrees\": eval})\n",
    "    degrees_df[\"connect_thresh\"] = CONNECT_THRESH\n",
    "    # Keep only the rows corresponding to \"good\" sessions\n",
    "    degrees_df = nb_utils.keep_rows_from_good_sessions(degrees_df, animal_dataset)\n",
    "else:\n",
    "    print(\n",
    "        \"Degree distributions for \"\n",
    "        + \"similarity_method={} and \".format(SIMILARITY_METHOD)\n",
    "        + \"connect_thresh={:.1f} couldn't be loaded\".format(CONNECT_THRESH)\n",
    "    )\n",
    "    degrees_df = None"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "degrees_df.head()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Get number of neurons in each network from the corresponding dataset statistics file.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "statistics_df = pd.read_csv(\n",
    "    os.path.join(ANALYSIS_DIR, \"sttc_statistics.csv\".format(SIMILARITY_METHOD)),\n",
    "    converters={\"sim_counts\": eval, \"sim_bins\": eval},\n",
    ")\n",
    "# Keep only the rows corresponding to \"good\" sessions\n",
    "statistics_df = nb_utils.keep_rows_from_good_sessions(statistics_df, animal_dataset)\n",
    "statistics_df.head()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For each session, normalize the node degrees by dividing by the number of neurons in the network\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Group the data by animal_id and session_date\n",
    "grouped_degrees = degrees_df.groupby([\"animal_id\", \"session_date\"])\n",
    "grouped_statistics = statistics_df.groupby([\"animal_id\", \"session_date\"])\n",
    "\n",
    "# Loop over the groups and normalize the node degrees\n",
    "for (animal_id, session_date), group in grouped_degrees:\n",
    "    # Get the corresponding group in the statistics dataframe\n",
    "    statistics_group = grouped_statistics.get_group((animal_id, session_date))\n",
    "    # Get the number of nodes\n",
    "    n_nodes = statistics_group[\"num_neurons\"].values[0]\n",
    "    # Normalize the node degrees\n",
    "    degrees_df.loc[group.index, \"degrees\"] = group[\"degrees\"].apply(\n",
    "        lambda x: x / n_nodes\n",
    "    )\n",
    "\n",
    "degrees_df.head()"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Comparisons across different developmental windows\n"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The cell below defines the development windows we'll be interested in.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dev_stage_cuts = utils.get_default_dev_stage_cuts()\n",
    "dev_grouping = nb_utils.group_by_dev_interval(degrees_df, dev_stage_cuts)"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Degrees\n"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Degree distributions color-coded by developmental window and overlaid to one another:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "hist_range = (0.0, 1.0)\n",
    "\n",
    "fig, ax = plt.subplots(1, 1, figsize=(10, 4))\n",
    "cmap = colormaps[\"viridis\"]\n",
    "colors = iter(cmap(np.linspace(0, 1, len(dev_grouping.size()))))\n",
    "for label, df in dev_grouping:\n",
    "    ax = nb_plotting.plot_degree_histogram(\n",
    "        df,\n",
    "        color=next(colors),\n",
    "        range=hist_range,\n",
    "        label=label,\n",
    "        ax=ax,\n",
    "        log=True,\n",
    "        histtype=\"step\",\n",
    "    )\n",
    "ax.set(\n",
    "    title=\"Degree distribution (from {}) by development window\".format(\n",
    "        SIMILARITY_METHOD\n",
    "    ),\n",
    "    xlabel=\"Degree (normalized by the number of nodes)\",\n",
    ")\n",
    "ax.legend()\n",
    "plt.show()"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A parallel plot of degree distributions of each developmental window:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, ax = plt.subplots(\n",
    "    1, len(dev_stage_cuts) - 1, figsize=(16, 4), sharex=True, sharey=True\n",
    ")\n",
    "j = 0\n",
    "for label, df in dev_grouping:\n",
    "    ax[j] = nb_plotting.plot_degree_histogram(\n",
    "        df,\n",
    "        range=hist_range,\n",
    "        ax=ax[j],\n",
    "        log=True,\n",
    "        histtype=\"stepfilled\",\n",
    "    )\n",
    "    ax[j].set(title=label, xlabel=\"Degree (normalized by the number of nodes)\")\n",
    "\n",
    "    if j > 0:\n",
    "        ax[j].set_ylabel(\"\")\n",
    "\n",
    "    j += 1\n",
    "\n",
    "fig.suptitle(\n",
    "    \"Degree distribution (from {}) by development window\".format(SIMILARITY_METHOD)\n",
    ")\n",
    "\n",
    "plt.tight_layout()\n",
    "\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def pct_with_degree_above_threshold(dev_window, threshold):\n",
    "    num_greater = 0\n",
    "    num_total = 0\n",
    "    tmp_df = dev_grouping.get_group(dev_window)\n",
    "    for row in tmp_df[\"degrees\"]:\n",
    "        row = np.array(row)\n",
    "        num_greater += np.sum(row > threshold)\n",
    "        num_total += len(row)\n",
    "    return num_greater / num_total"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dev_window = \"PDay 15-24\"\n",
    "threshold = 0.5\n",
    "print(\"Percentage of nodes with degree > {} in {}:\".format(threshold, dev_window))\n",
    "print(\"\\t{:.2f}%\".format(pct_with_degree_above_threshold(dev_window, threshold) * 100))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dev_window = \"PDay 24-55\"\n",
    "threshold = 0.5\n",
    "print(\"Percentage of nodes with degree > {} in {}:\".format(threshold, dev_window))\n",
    "print(\"\\t{:.2f}%\".format(pct_with_degree_above_threshold(dev_window, threshold) * 100))"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Degree distributions of some random graphs\n",
    "\n",
    "How to these degree distributions compare with the degree distributions of some random graphs?\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, ax = plt.subplots(1, 4, figsize=(16, 4), sharey=True, sharex=True)\n",
    "\n",
    "kwargs = {\"bins\": 15, \"color\": PALETTE.anthrazit_hell, \"log\": True}\n",
    "\n",
    "# Get number of nodes from young mouse example\n",
    "n = young_mouse_example_sg.G_nx.number_of_nodes()\n",
    "\n",
    "# 1 - Erdos-Renyi\n",
    "p = 0.025\n",
    "G = nx.fast_gnp_random_graph(n, p)\n",
    "\n",
    "degrees = [G.degree()[node] / len(G.nodes()) for node in G.nodes()]\n",
    "nb_plotting.plot_histogram(\n",
    "    degrees,\n",
    "    ax=ax[0],\n",
    "    **kwargs,\n",
    ")\n",
    "nb_plotting.draw_median_line(degrees, ax=ax[0])\n",
    "ax[0].set(title=\"Erdos-Renyi\", xlabel=\"Normalized degree\")\n",
    "\n",
    "# 2 - 3D grid graph\n",
    "cr_n = int(n ** (1 / 3))\n",
    "G = nx.grid_graph(dim=(cr_n, cr_n, cr_n))\n",
    "\n",
    "degrees = [G.degree()[node] / len(G.nodes()) for node in G.nodes()]\n",
    "nb_plotting.plot_histogram(\n",
    "    degrees,\n",
    "    ax=ax[1],\n",
    "    **kwargs,\n",
    ")\n",
    "nb_plotting.draw_median_line(degrees, ax=ax[1])\n",
    "ax[1].set(title=\"3D grid graph\", xlabel=\"Normalized degree\")\n",
    "\n",
    "# 3 - 3D Random geometric\n",
    "radius = 0.2\n",
    "G = nx.random_geometric_graph(n, radius, dim=3)\n",
    "\n",
    "degrees = [G.degree()[node] / len(G.nodes()) for node in G.nodes()]\n",
    "nb_plotting.plot_histogram(\n",
    "    degrees,\n",
    "    ax=ax[2],\n",
    "    **kwargs,\n",
    ")\n",
    "nb_plotting.draw_median_line(degrees, ax=ax[2])\n",
    "ax[2].set(title=\"3D Random geometric\", xlabel=\"Normalized degree\")\n",
    "\n",
    "# 4 - Dual Barabasi-Albert\n",
    "m1 = 1\n",
    "m2 = 3\n",
    "p = 0.75\n",
    "G = nx.dual_barabasi_albert_graph(n, m1, m2, p)\n",
    "\n",
    "degrees = [G.degree()[node] / len(G.nodes()) for node in G.nodes()]\n",
    "nb_plotting.plot_histogram(\n",
    "    degrees,\n",
    "    ax=ax[3],\n",
    "    **kwargs,\n",
    ")\n",
    "nb_plotting.draw_median_line(degrees, ax=ax[3])\n",
    "ax[3].set(title=\"Dual Barabasi-Albert\", xlabel=\"Normalized degree\")\n",
    "\n",
    "plt.tight_layout()\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3.10.4 ('neural-graphs-mice-development')",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.13"
  },
  "orig_nbformat": 4,
  "vscode": {
   "interpreter": {
    "hash": "d9c38bc351936cd29f41b027944acd72081f5aecde362cbc64307b9dc28d2922"
   }
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
