{
 "cells": [
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Similarity matrix statistics\n",
    "\n",
    "In this notebook we will exploring the distribution of correlation, Z-score, and STTC values for pairs of neurons in the dataset. In particular, we will be interested in observing any pattern changes throughout the development of the mice.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import os\n",
    "\n",
    "import matplotlib.pyplot as plt\n",
    "import nb_config\n",
    "import nb_plotting\n",
    "import nb_utils\n",
    "import numpy as np\n",
    "import pandas as pd\n",
    "from matplotlib import colormaps\n",
    "\n",
    "from ngmd import dataset, utils\n",
    "\n",
    "%reload_ext autoreload\n",
    "%autoreload 2\n",
    "\n",
    "# Dataset path as a global variable. Remember to set the correct path to the\n",
    "# dataset in `notebooks/nb_config.py` or set it directly here\n",
    "DATA_DIR = nb_config.DATA_DIR\n",
    "\n",
    "# Path to directory with pre-computed analysis files\n",
    "ANALYSIS_DIR = os.path.join(DATA_DIR, \"analysis_files\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Loading the animal dataset\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "animal_dataset = dataset.AnimalDataset(DATA_DIR)"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Statistics of the dataset have been compiled outside this notebook, by running the script `scripts/dataset_statistics.py`.\n",
    "\n",
    "Each row in the resulting output table represents an experiment session for a particular animal. These rows are identified by an animal ID (`animal_id`), a session date (`session_date`), and the development stage (`dev_stage`) at which that particular animal was in that session.\n",
    "\n",
    "The column `num_neurons` denotes the number of neurons identified from the calcium fluorescence measurements, which is the same as the number of rows (or columns) in the correlation matrix stored for the corresponding session.\n",
    "\n",
    "The histogram of neuron similarity values (correlations or z-scores) is represented here by bins (`bins`) and respective counts (`counts`). The value of `bins` is the same for all experiment sessions, working as a global reference for computing similarity value counts.\n",
    "\n",
    "Finally, the last four columns, `min_sim_val`, `max_sim_val`, `avg_sim_val`, and `std_sim_val`, record each session's minimum, maximum, average, and standard deviation of the _valid, non-trivial_ similarity values (i.e., excluding neuron self-similarity and non-numerical entries).\n",
    "\n",
    "_Remark:_ We discard rows corresponding to \"bad\" sessions. These are sessions that were marked as bad during the analysis because their metric was to far away from what one would see the days right before and after.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "corrs_statistics = pd.read_csv(\n",
    "    os.path.join(ANALYSIS_DIR, \"corrs_statistics.csv\"),\n",
    "    converters={\"sim_counts\": eval, \"sim_bins\": eval},\n",
    ")\n",
    "# Keep only the rows corresponding to \"good\" sessions\n",
    "corrs_statistics = nb_utils.keep_rows_from_good_sessions(\n",
    "    corrs_statistics, animal_dataset\n",
    ")\n",
    "corrs_statistics.head()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sttc_statistics = pd.read_csv(\n",
    "    os.path.join(ANALYSIS_DIR, \"sttc_statistics.csv\"),\n",
    "    converters={\"sim_counts\": eval, \"sim_bins\": eval},\n",
    ")\n",
    "# Keep only the rows corresponding to \"good\" sessions\n",
    "sttc_statistics = nb_utils.keep_rows_from_good_sessions(sttc_statistics, animal_dataset)\n",
    "sttc_statistics.head()"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Global statistics\n",
    "\n",
    "### Correlation values\n",
    "\n",
    "Let us first take a look at how the correlation values are globally distributed across the dataset.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Plot histogram\n",
    "ax = nb_plotting.plot_sim_histogram(\n",
    "    corrs_statistics,\n",
    "    bar=False,\n",
    ")\n",
    "ax.set_title(\"Histogram of correlation values across the whole dataset\")\n",
    "\n",
    "# Reset x-axis limits\n",
    "# min_x = corrs_statistics['min_sim_val'].min()  # Use the data min\n",
    "# max_x = corrs_statistics['max_sim_val'].max()  # and max\n",
    "min_x = -0.1  # Or, alternatively, use a fixed range\n",
    "max_x = 1\n",
    "ax.set_xlim(min_x, max_x)\n",
    "\n",
    "# Reset y-axis limits (frequency values)\n",
    "min_y = 0.0001  # 1 in 10,000\n",
    "max_y = 1\n",
    "ax.set_ylim(min_y, max_y)\n",
    "ax.set_yscale(\"log\")\n",
    "\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### STTC values\n",
    "\n",
    "And now how the STTC values are distributed:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Plot histogram\n",
    "ax = nb_plotting.plot_sim_histogram(\n",
    "    sttc_statistics,\n",
    "    bar=False,\n",
    ")\n",
    "ax.set_title(\"Histogram of STTC values across the whole dataset\")\n",
    "\n",
    "# Reset x-axis limits\n",
    "# min_x = corrs_statistics['min_sim_val'].min()  # Use the data min\n",
    "# max_x = corrs_statistics['max_sim_val'].max()  # and max\n",
    "min_x = -0.1  # Or, alternatively, use a fixed range\n",
    "max_x = 1\n",
    "ax.set_xlim(min_x, max_x)\n",
    "\n",
    "# Reset y-axis limits (frequency values)\n",
    "min_y = 0.0001  # 1 in 10,000\n",
    "max_y = 1\n",
    "ax.set_ylim(min_y, max_y)\n",
    "ax.set_yscale(\"log\")\n",
    "\n",
    "plt.show()"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Grouping by developmental stage windows\n",
    "\n",
    "Scientifically, we are interested in how the patterns in neuronal correlations change throughout the development of the mice. However, collecting data every day of a mice's life is expensive, so we do not have a uniform representation of development stages in the dataset.\n",
    "\n",
    "Have a look in the cell below at the histogram of development days (a.k.a. \"PDays\") present in the dataset. Most experiment sessions took place with mice between 15 and 40 days old. Representatives tend to decrease in number as we increase the age. Some development windows, such as around PDay = 80 or between PDays 100 and 120, have no representatives at all.\n",
    "\n",
    "This inhomogeneity leads us to consider development windows of varied sizes with which to slice the dataset. Essentially, for the earliest and better-represented developmental stages, we take 10-day-long windows, starting from PDay=15. Then, for later PDays we take windows that are wide enough to be sure to contain representative experimental sessions. The orange lines in the histogram below illustrate the borders of the development windows that we end up using.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Use default developmental stage windows\n",
    "dev_stage_cuts = utils.get_default_dev_stage_cuts()\n",
    "dev_stage_labels = nb_utils.labels_from_cuts(dev_stage_cuts)\n",
    "print(\"Developmental stage cuts: {}\".format(dev_stage_cuts))\n",
    "print(\"Corresponding developmental window labels: {}\".format(dev_stage_labels))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.hist(corrs_statistics[\"dev_stage\"], bins=51, color=\"black\", alpha=0.6)\n",
    "for val in dev_stage_cuts[:-1]:\n",
    "    plt.axvline(val, color=\"orange\", alpha=0.6)\n",
    "plt.axvline(\n",
    "    dev_stage_cuts[-1],\n",
    "    color=\"orange\",\n",
    "    alpha=0.6,\n",
    "    label=\"Development stage bins\",\n",
    ")\n",
    "plt.legend()\n",
    "plt.title(\"Developmental stages\")\n",
    "plt.xlabel(\"PDay\")\n",
    "plt.ylabel(\"Count\")\n",
    "plt.show()"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The next cell implements a shortcut to group rows of the statistics table according to the developmental windows defined above.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "corrs_dev_grouping = nb_utils.group_by_dev_interval(\n",
    "    corrs_statistics, bins=dev_stage_cuts\n",
    ")\n",
    "\n",
    "sttc_dev_grouping = nb_utils.group_by_dev_interval(sttc_statistics, bins=dev_stage_cuts)"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can use the developmental stage grouping to assess how many different mice are represented in each window. Note that the windows themselves we defined so that there would be a(n almost) uniform representation of experimental sessions across windows. Indeed, the number of sessions within each window is very similar, varying between 80 and 85.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "number_of_sessions = {}\n",
    "for label, df in corrs_dev_grouping:\n",
    "    number_of_sessions[label] = len(df)\n",
    "\n",
    "print(\"Number of sessions in each developmental window:\")\n",
    "print(\n",
    "    \"\\n\".join([\"\\t{}: {}\".format(key, val) for key, val in number_of_sessions.items()])\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This does not mean that each window also contains the same number of mice.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "number_of_mice = {}\n",
    "for label, df in corrs_dev_grouping:\n",
    "    number_of_mice[label] = len(df[\"animal_id\"].unique())\n",
    "\n",
    "print(\"Number of different mice in each developmental window:\")\n",
    "print(\"\\n\".join([\"\\t{}: {}\".format(key, val) for key, val in number_of_mice.items()]))"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Correlation values per development window\n",
    "\n",
    "With a developmental window grouping, we can also overlay the histograms of correlation values within each development window to see if any distinguishable patterns are revealed as we move from one window to another.\n",
    "\n",
    "The PDay windows in the histogram below are color-coded, from shades of blue for young ages, passing by shades of green as age progresses, up until yellow for the later days of development.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, ax = plt.subplots(1, 1, figsize=(8, 4))\n",
    "cmap = colormaps[\"viridis\"]\n",
    "colors = iter(cmap(np.linspace(0, 1, len(corrs_dev_grouping.size()))))\n",
    "for label, df in corrs_dev_grouping:\n",
    "    ax = nb_plotting.plot_sim_histogram(\n",
    "        df,\n",
    "        color=next(colors),\n",
    "        label=label,\n",
    "        ax=ax,\n",
    "    )\n",
    "ax.set_title(\"Distribution of correlation values by development window\")\n",
    "ax.legend()\n",
    "\n",
    "# Reset x-axis\n",
    "corrs_limits = (-0.1, 1.0)\n",
    "# corrs_limits = (-0.1, 0.1)\n",
    "ax.set_xlim(*corrs_limits)\n",
    "\n",
    "# Reset y-axis\n",
    "freq_limits = (1e-4, 1)  # From 1 in 10,000 to 1\n",
    "# freq_limits = (1e-2, 1)  # From 1 in 100 to 1\n",
    "ax.set_ylim(*freq_limits)\n",
    "ax.set_yscale(\"log\")\n",
    "ax.set_ylabel(\"Frequency\")\n",
    "\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And here is the same information, but with developmental windows split into subplots.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, ax = plt.subplots(\n",
    "    1, len(dev_stage_cuts) - 1, figsize=(16, 4), sharex=True, sharey=True\n",
    ")\n",
    "j = 0\n",
    "for label, df in corrs_dev_grouping:\n",
    "    ax[j] = nb_plotting.plot_sim_histogram(\n",
    "        df,\n",
    "        ax=ax[j],\n",
    "    )\n",
    "    ax[j].set_title(label)\n",
    "    ax[j].set_xlim(*corrs_limits)\n",
    "    ax[j].set_xlabel(\"Correlation\")\n",
    "    ax[j].set_ylim(*freq_limits)\n",
    "    ax[j].set_yscale(\"log\")\n",
    "    if j == 0:\n",
    "        ax[j].set_ylabel(\"Frequency\")\n",
    "    else:\n",
    "        ax[j].set_ylabel(\"\")\n",
    "    j += 1\n",
    "\n",
    "plt.tight_layout()\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### STTC values per development window\n",
    "\n",
    "And the STTC values:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, ax = plt.subplots(1, 1, figsize=(8, 4))\n",
    "cmap = colormaps[\"viridis\"]\n",
    "colors = iter(cmap(np.linspace(0, 1, len(sttc_dev_grouping.size()))))\n",
    "for label, df in sttc_dev_grouping:\n",
    "    ax = nb_plotting.plot_sim_histogram(\n",
    "        df,\n",
    "        color=next(colors),\n",
    "        label=label,\n",
    "        ax=ax,\n",
    "    )\n",
    "ax.set_title(\"Distribution of STTC values by development window\")\n",
    "ax.legend()\n",
    "\n",
    "# Reset x-axis\n",
    "sttc_limits = (-0.1, 1.0)\n",
    "# sttc_limits = (-0.1, 0.1)\n",
    "ax.set_xlim(*sttc_limits)\n",
    "\n",
    "# Reset y-axis\n",
    "freq_limits = (1e-4, 1)  # From 1 in 10,000 to 1\n",
    "# freq_limits = (1e-2, 1)  # From 1 in 100 to 1\n",
    "ax.set_ylim(*freq_limits)\n",
    "ax.set_yscale(\"log\")\n",
    "ax.set_ylabel(\"Frequency\")\n",
    "\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, ax = plt.subplots(\n",
    "    1, len(dev_stage_cuts) - 1, figsize=(16, 4), sharex=True, sharey=True\n",
    ")\n",
    "j = 0\n",
    "for label, df in sttc_dev_grouping:\n",
    "    ax[j] = nb_plotting.plot_sim_histogram(\n",
    "        df,\n",
    "        ax=ax[j],\n",
    "        bar=False,\n",
    "    )\n",
    "    ax[j].set_title(label)\n",
    "    ax[j].set_xlim(*sttc_limits)\n",
    "    ax[j].set_xlabel(\"STTC\")\n",
    "    ax[j].set_ylim(*freq_limits)\n",
    "    ax[j].set_yscale(\"log\")\n",
    "    if j == 0:\n",
    "        ax[j].set_ylabel(\"Frequency\")\n",
    "    else:\n",
    "        ax[j].set_ylabel(\"\")\n",
    "    j += 1\n",
    "\n",
    "plt.tight_layout()\n",
    "plt.show()"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "neural-graphs-mice-development",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.13"
  },
  "vscode": {
   "interpreter": {
    "hash": "d9c38bc351936cd29f41b027944acd72081f5aecde362cbc64307b9dc28d2922"
   }
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
